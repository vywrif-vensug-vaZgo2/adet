# Welcome to Autonomous Data Extraction Tool

## Description

Autonomous Data Extraction Tool will retrive data by issuing RESTful requests to Springer Nature API service at **http://api.springernature.com** and store data in AperatumX Database system.

---

## Technologies Used

Autonomous Data Extraction Tool is java based. 

---

## Springer Nature API

 

1. **Springer Nature Meta API** - Provides new versioned metadata for 14 million online documents (e.g., journal articles, book chapters, protocols).

2. **Springer Nature Metadata API** - Provides metadata for 14 million online documents (e.g., journal articles, book chapters, protocols).

3. **Springer Nature Open Access API** - Provides metadata and full-text content where available for more than 649,000 online documents from Springer Nature open access xml, including BMC and SpringerOpen journals.

4. **Integro API** - Provides a means to access information about Springer Nature’s journals (e.g., Information about the journal’s title, publisher, DOI, subject groups, dates of publication). See examples at the bottom of Adding Constraints.

---

## Requests to API

Requests must always include both a **Collection** and a **ResultFormat**.

1. The Collection identifies what repository should be searched for results.

2. The ResultFormat indicates how data should be returned by the API.

**Example**

http://api.springernature.com/metadata/json/doi/10.1007/s11276-008-0131-4?api_key=yourKeyHere

1. Collection = metadata

2. ResultFormat = json

To see all Valid Collection and ResultFormat parameters, please visit https://dev.springernature.com/restfuloperations or go to https://dev.springernature.com and click on RESTful Operations

---

## Steps to request access to API key 

1. go to **https://dev.springernature.com**
2. Click on **Documentation**.
3. Scroll down and **click on "Apply for API key"**.

---

## API Response XML and JSON

### Import library

1. **import lombok library.** 
import lombok.Data;  
and import lombok.extern.slf4j.Slf4j;

2. **import the HashMap class**- which allows us to store key and value pair.
import java.util.HashMap;

3. **import java.util.Map;**- which allow a map's contents to be viewed as a set of keys, collection of values, or set of key-value mappings.

### XML response

1. **public class Creator** - private Map<String, Object> additionalProperties = new HashMap<String, Object>();   //HashMap

2. **public class Facet** - private Map<String, Object> additionalProperties = new HashMap<String, Object>();

3. **public class Record**-     private Map<String, Object> additionalProperties = new HashMap<String, Object>();

4. **public class Result**- return total; return start; return pageLength; return recordsDisplayed;

5. **public class Springer**- return apiMessage; return query; return apiKey; return result; return records; return facets;

6. **public class Url**- return format; return platform; return value;

7. **public class Value**- return value; return count;

### JSON response

1. **public class Codebeautify** - return apiMessage; return query; return apiKey;

2. **public class SPJsonResponse** - return apiMessage; return query; return apiKey;